"""
mapper.py
=========================

Mapper class definition for plotting US choropleth maps based on gridded data.

"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

__author__ = "Tyler Huntington"

class Mapper:
    def __init__(self, data, n_bins=20, *args, **kwargs):
        """Constructor

        :param data: Array-like gridded data to be mapped, or valid filepath
            to .csv file containing such data.
        :param int n_bins: number of discrete bins into which raster
        values should be grouped for choropleth mapping.
        :param args:
        :param kwargs:
        """
        self.n_bins = n_bins

        if isinstance(data, np.ndarray):
            self.data = data
        elif isinstance(data, pd.DataFrame):
            self.data = data.values
        elif os.path.exists(data):
            df = pd.read_csv(data)
            self.data = df.values
        else:
            raise Exception(
                """
                Invalid Data Source! Please provide pandas DataFrame or valid
                filepath to .csv file with gridded raster values.
                """
            )

    def _bin_raster_vals(self, bifurcate_val=0,
                         transform='log',
                         *args, **kwargs):
        """Groups raster values along a continuous range to into discrete bins

        :param int bifurcate_val: value used to split distribution of
            raster values before separately binning each portion of the range.
            Default is `None`, which results in no bifurcation before binning.
        :param str transform: transformation function to apply to raster values.
            Default is `'log'`.
        :param args:
        :param kwargs:
        :return:
        """

        if transform == 'log':
            self.data = np.where(
                self.data >= 0,
                np.log(self.data),
                -1 * np.log(abs(self.data))
            )

        max_val = self.data.max()
        self.data[np.isinf(self.data)] = max_val + 1
        min_val = self.data.min()


        self.data[np.isinf(self.data)] = max_val + 1

        if bifurcate_val is not None:
            # first check that number of bins is even, which is required for
            # bifurcated binning procedure. If odd, increment n_bins by one.
            if self.n_bins % 2 != 0:
                self.n_bins += 1

            left_bins = np.linspace(min_val, bifurcate_val, self.n_bins//2 + 1)
            right_bins = np.linspace(bifurcate_val, max_val, self.n_bins//2 + 1)
            bins = np.union1d(left_bins, right_bins)

        else:
            bins = np.linspace(min_val, max_val, self.n_bins + 2)

        self.data = np.digitize(self.data, bins, right=True)
        self.data[self.data == (self.n_bins + 1) ] = 0

    def make_choropleth_map(self, outfile='choropleth_map.png', *args, **kwargs):
        """Generates choropleth map and exports to .png file

        :param outfile: filepath to which map should be exported
        :param args:
        :param kwargs:
        :return:
        """

        left_y_lab = kwargs.pop('left_y_lab', 'Battery-Electric Suitability')
        right_y_lab = kwargs.pop('right_y_lab', 'Diesel Suitability')

        fig = plt.figure(figsize=(8, 4.5))
        ax = fig.add_axes([0.04, 0.05, 0.9, 0.9])

        # Lambert Conformal map of lower 48 states.
        proj = 'lcc'

        map = Basemap(
            llcrnrlon=-121.8,
            llcrnrlat=18.4,
            urcrnrlon=-59.15,
            urcrnrlat=52.5,
            projection=proj,
            resolution='c',
            lat_0=40, lon_0=-97,
            lat_1=33, lat_2=45,
        )

        # draw state boundaries
        map.readshapefile(
            'data/geo/cb_2018_us_state_500k', name='states', drawbounds=True,
            linewidth=0.2
        )

        lower_data = np.where(
            self.data < self.n_bins // 2,
            self.data,
            0
        )
        lower_cmap = plt.get_cmap('Blues', self.n_bins // 2)
        lower_cmap.set_under('k', alpha=0)

        upper_data = np.where(
            self.data >= self.n_bins // 2,
            self.data,
            0
        )
        upper_cmap = plt.get_cmap('Reds', self.n_bins // 2)
        upper_cmap.set_under('k', alpha=0)

        alpha = 1
        upper = map.imshow(
            upper_data.T,
            cmap=upper_cmap,
            vmin=self.n_bins//2,
            alpha=alpha
        )

        lower = map.imshow(
            lower_data.T,
            cmap=lower_cmap,
            vmin=1,
            alpha=alpha
        )
        # draw state boundaries
        map.readshapefile(
            'data/geo/cb_2018_us_state_500k', name='states', drawbounds=True,
            linewidth=0.2
        )

        cb_up = map.colorbar(upper, location='right', size='4%', pad='14%')
        cb_lo = map.colorbar(lower, location='left', size='4%')

        cb_lo.ax.tick_params(size=0)
        cb_lo.ax.tick_params(tickdir='in')
        cb_lo.ax.tick_params(labelleft=True)
        cb_lo.ax.tick_params(labelright=False)
        cb_up.ax.tick_params(size=0)

        ax.set_ylabel(left_y_lab, labelpad=62)

        # instantiate second y-axis to add right-side colorbar label
        ax2 = ax.twinx()
        for pos in ['left', 'right', 'top', 'bottom']:
            ax2.spines[pos].set_visible(False)

        # Turn off tick labels
        plt.yticks([], [])
        ax2.set_yticklabels([])
        ax2.set_xticklabels([])

        ax2.set_ylabel(right_y_lab)

        # plt.show()
        plt.savefig(outfile, quality=100, dpi=200)

def main():

    fp = 'data/test_NOX.csv'
    mapper = Mapper(data=fp)
    mapper._bin_raster_vals()
    mapper.make_choropleth_map(outfile='output/freight_choropleth.png')

if __name__ == '__main__':
    main()

